MAKEFLAGS += -rR
CC=gcc
FLAGS=-O3 -I include -Wall -Wextra -MMD -O -D_GNU_SOURCE
CFLAGS=$(FLAGS) -std=c99 `pkg-config --cflags MagickCore`
LDFLAGS=$(FLAGS) `pkg-config --libs MagickCore` -lm
# turns on extra information about which loop have been vectorised
# CFLAGS+= -ftree-vectorizer-verbose=2
COMPILE = $(CC) -c $(CFLAGS)
CREATE_DEST_DIR = mkdir -p $(dir $@)

BIN_DIR=bin
FVSCBIN=$(BIN_DIR)/fvs_main
FVSCTEST=$(BIN_DIR)/fvs_test
MAIN=fvs_main.c
TEST=test_main.c
SRCS=export.c floatfield.c image.c imageenhance.c imagemanip.c img_base.c import.c matching.c minutia.c

all: $(FVSCBIN)
test: $(FVSCTEST)

$(BIN_DIR)/%.o : %.c
	@$(CREATE_DEST_DIR)
	$(COMPILE) -o $@ $<

$(FVSCBIN) : $(addprefix $(BIN_DIR)/, $(SRCS:.c=.o)) $(addprefix $(BIN_DIR)/, $(MAIN:.c=.o))
	$(CC) -o $@ $^ $(LDFLAGS)
$(FVSCTEST) : $(addprefix $(BIN_DIR)/, $(SRCS:.c=.o)) $(addprefix $(BIN_DIR)/, $(TEST:.c=.o))
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf $(BIN_DIR)/*

-include $(addprefix $(BIN_DIR)/, $(SRCS:.c=.d)) $(addprefix $(BIN_DIR)/, $(MAIN:.c=.d))
