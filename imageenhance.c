/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
########################################################################*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "imagemanip.h"


/*
** jdh: image enhancement part. This enhancement algorithm is specialized
** on fingerprint images. It marks regions that are not to be used with a
** special color in the mask. Other pixels are improved to that ridges can
** be clearly separated using a threshold value.
** The algorithm produces a ridges direction field image and a mask that
** masks out the unusable areas or areas that are likely to contain no
** fingerprint as well as border of fingerprint areas.
**
** Ideas have been found in small articles:
** 1 - Fingerprint Enhancement: Lin Hong, Anil Jain, Sharathcha Pankanti,
**     and Ruud Bolle. [Hong96]
** 2 - Fingerprint Image Enhancement, Algorithm and Performance Evaluation:
**     Lin Hong, Yifei Wan and Anil Jain. [Hong98]
**
** The enhancement is performed using several steps as detailed in (2)
**  A - Normalization
**  B - Compute Orientation
**  C - Compute Frequency
**  D - Compute Region Mask
**  E - Filter
**
*/


/* {{{ Filter the parts and improve the ridges */

/*
** We apply a Gabor filter over the image using the direction and the
** frequency computer in the steps before. The even symetric gabor function
** used is the following
**
**                    / 1|x'²   y'²|\
** h(x,y:phi,f) = exp|- -|--- + ---| |.cos(2.PI.f.x')
**                    \ 2|dx²   dy²|/
**
** x' =  x.cos(phi) + y.sin(phi)
** y' = -x.sin(phi) + y.cos(phi)
**
** Its value is based on empirical data, we choose to set it to 4.0 at first.
** The bigger the value is, mre resistant to noise becomes the algorithm,
** but more likely will he procude spurious ridges.
**
** Let:
**  G be the normalized image
**  O the orientation image
**  F the frequency image
**  R the removable mask image
**  E the resulting enhanced image
**  Wg the size of the gabor filter
**
**          / 255                                          if R(i,j) = 0
**         |
**         |  Wg/2    Wg/2
**         |  ---     ---
** E(i,j)= |  \       \
**         |   --      --  h(u,v:O(i,j),F(i,j)).G(i-u,j-v) otherwise
**         |  /       /
**          \ ---     ---
**            u=-Wg/2 v=-Wg/2
**
*/
/* helper function computing the gabor filter factor r2 is r*r */
inline FvsFloat_t EnhanceGabor(FvsFloat_t x, FvsFloat_t y, FvsFloat_t phi, FvsFloat_t f, FvsFloat_t r2)
{
    FvsFloat_t dy2 = 1.0/r2;
    FvsFloat_t dx2 = 1.0/r2;
    FvsFloat_t x2, y2;
    phi += M_PI/2;
    x2 = -x*sin(phi) + y*cos(phi);
    y2 =  x*cos(phi) + y*sin(phi);
    return exp(-0.5*(x2*x2*dx2 + y2*y2*dy2))*cos(2*M_PI*x2*f);
}

static FvsError_t ImageEnhanceFilter(
    FvsImage_t        normalized,
    const FvsImage_t  mask,
    const FvsFloat_t* orientation,
    const FvsFloat_t* frequence,
    FvsFloat_t        radius)
{
    FvsInt_t Wg2 = 8; /* from -5 to 5 are 11 */
    FvsInt_t i,j, u,v;
    FvsError_t nRet  = FvsOK;
    FvsImage_t enhanced = NULL;

    FvsInt_t w        = ImageGetWidth (normalized);
    FvsInt_t h        = ImageGetHeight(normalized);
    FvsInt_t pitchG   = ImageGetPitch (normalized);
    FvsByte_t* pG     = ImageGetBuffer(normalized);
    FvsFloat_t sum, f, o;

    /* take square */
    radius = radius*radius;

    enhanced = ImageCreate();
    if (enhanced==NULL || pG==NULL)
        return FvsMemory;
    if (nRet==FvsOK)
        nRet = ImageSetSize(enhanced, w, h);
    if (nRet==FvsOK)
    {
        FvsInt_t pitchE  = ImageGetPitch (enhanced);
        FvsByte_t* pE    = ImageGetBuffer(enhanced);
        if (pE==NULL)
            return FvsMemory;
        (void)ImageClear(enhanced);
        for (j = Wg2; j < h-Wg2; j++)
            for (i = Wg2; i < w-Wg2; i++)
            {
                if (mask==NULL || ImageGetPixel(mask, i, j)!=0)
                {
                    sum = 0.0;
                    o = orientation[i+j*w];
                    f = frequence[i+j*w];
                    for (v = -Wg2; v <= Wg2; v++)
                        for (u = -Wg2; u <= Wg2; u++)
                        {
                            sum += EnhanceGabor((FvsFloat_t)u, (FvsFloat_t)v, o, f, radius)
                                * pG[(i-u)+(j-v)*pitchG];
                        }
                    /* printf("%6.1f ", sum);*/
                    if (sum>255.0) sum = 255.0;
                    if (sum<0.0)   sum = 0.0;
                    pE[i+j*pitchE] = (uint8_t)sum;
                }
            }
        nRet = ImageCopy(normalized, enhanced);
    }
    (void)ImageDestroy(enhanced);
    return nRet;
}

/* }}} */

/* Enhance a fingerprint image */
FvsError_t ImageEnhanceGabor(FvsImage_t image, const FvsFloatField_t direction,
            const FvsFloatField_t frequency, const FvsImage_t mask, const FvsFloat_t radius)
{
    FvsError_t nRet = FvsOK;
    FvsFloat_t * image_orientation = FloatFieldGetBuffer(direction);
    FvsFloat_t * image_frequence   = FloatFieldGetBuffer(frequency);

    if (image_orientation==NULL || image_frequence==NULL)
        return FvsMemory;

    nRet = ImageEnhanceFilter(image, mask, image_orientation, image_frequence, radius);
    return nRet;
}




/* defines to facilitate reading */
#define P(x,y)      ((x)+(y)*pitch)
#define P1  p[P(x  ,y  )]
#define P2  p[P(x  ,y-1)]
#define P3  p[P(x+1,y-1)]
#define P4  p[P(x+1,y  )]
#define P5  p[P(x+1,y+1)]
#define P6  p[P(x  ,y+1)]
#define P7  p[P(x-1,y+1)]
#define P8  p[P(x-1,y  )]
#define P9  p[P(x-1,y-1)]

// written by petertu
//This function recursively removes Spurs and isolated lines
FvsInt_t RemoveSpursRec(FvsImage_t image,FvsInt_t x,FvsInt_t y,FvsInt_t lastpos,FvsInt_t distance)
{
  FvsInt_t w       = ImageGetWidth(image);
  FvsInt_t pitch   = ImageGetPitch(image);
  FvsByte_t* p     = ImageGetBuffer(image);
	FvsInt_t sum, ret;

	sum = P2+P3+P4+P5+P6+P7+P8+P9;

	//ignore image border
	if(x == 1 || y == 1 || x == w-2 || y == w-2)
	{

		if (distance < 15)
		{
			P1 = 0;
			return 1;
		}
		else

			return 0;
	}
	//went far enough
	else if(distance > 20)
	{
		return 0;
	}
	//normal line or start of Spur
	else if(sum == 510 || distance == 0)
	{
		if     (P2 == 0xFF && P(x  ,y-1) != lastpos)
			ret = RemoveSpursRec(image, x, y-1,P(x,y),distance +1);
		else if(P3 == 0xFF && P2 == 0 && P4 == 0 && P(x+1,y-1) != lastpos)
			ret = RemoveSpursRec(image, x+1, y-1,P(x,y),distance +1);
		else if(P4 == 0xFF && P(x+1,y  ) != lastpos)
			ret = RemoveSpursRec(image, x+1, y,P(x,y),distance +1);
		else if(P5 == 0xFF && P4 == 0 && P6 == 0 && P(x+1,y+1) != lastpos)
			ret = RemoveSpursRec(image, x+1, y+1,P(x,y),distance +1);
		else if(P6 == 0xFF && P(x  ,y+1) != lastpos)
			ret = RemoveSpursRec(image, x, y+1,P(x,y),distance +1);
		else if(P7 == 0xFF && P6 == 0 && P8 == 0 && P(x-1,y+1) != lastpos)
			ret = RemoveSpursRec(image, x-1, y+1,P(x,y),distance +1);
		else if(P8 == 0xFF && P(x-1,y  ) != lastpos)
			ret = RemoveSpursRec(image, x-1, y,P(x,y),distance +1);
		else // P9 == 0xFF && P(x-1,y-1) != lastpos
			ret = RemoveSpursRec(image, x-1, y-1,P(x,y),distance +1);
	}
	// isolated line, may be of length 20
	else if (sum == 255)
	{
		P1 = 0;
		return 1;
	}
	//Spur - may be of length 15
	else if (sum > 510 && distance < 15)
	{
		p[lastpos] = 0;
		if((P9+P2+P3 > 0 && P7+P6+P5 > 0) || (P9+P8+P7 > 0 && P3+P4+P5 > 0))
			{}//Pixel is a relevant connector, leave it at 1
		else
			P1 = 0;
		return 1;
	}
	//shouldn't get there
	else
	{
		return 0;
	}

	if(ret == 1)
	{
		P1 = 0;
		return 1;
	}
	else
	{
		return 0;
	}
}

// written by petertu
//This function recursively finds circles
FvsInt_t FindCirclesRec(FvsImage_t image,FvsInt_t x,FvsInt_t y,FvsInt_t lastpos, FvsInt_t *targetpos, FvsInt_t distance)
{
  FvsInt_t pitch   = ImageGetPitch(image);
  FvsByte_t* p     = ImageGetBuffer(image);
	FvsInt_t sum,ret;

	sum = P2+P3+P4+P5+P6+P7+P8+P9;
	//either a normal line segment (sum == 510) or a false minitua
	if(distance > 15)
	{
		*targetpos = -1;
		return -1;
	}
	else if(sum == 510 || (sum > 510 && ((P2 && P3) || (P3 && P4) || (P4 && P5) || (P5 && P6) || (P6 && P7) || (P7 && P8) || (P8 && P9) || (P9 && P2))))
	{
		//P2,P4,P6,P8 first
		if     (P2 == 0xFF && P(x  ,y-1) != lastpos)
			ret = FindCirclesRec(image, x, y-1,P(x,y),targetpos,distance +1);
		else if(P4 == 0xFF && P(x+1,y  ) != lastpos)
			ret = FindCirclesRec(image, x+1, y,P(x,y),targetpos,distance +1);
		else if(P6 == 0xFF && P(x  ,y+1) != lastpos)
			ret = FindCirclesRec(image, x, y+1,P(x,y),targetpos,distance +1);
		else if(P8 == 0xFF && P(x-1,y  ) != lastpos)
			ret = FindCirclesRec(image, x-1, y,P(x,y),targetpos,distance +1);
		else if(P3 == 0xFF && P2 == 0 && P4 == 0 && P(x+1,y-1) != lastpos)
			ret = FindCirclesRec(image, x+1, y-1,P(x,y),targetpos,distance +1);
		else if(P5 == 0xFF && P4 == 0 && P6 == 0 && P(x+1,y+1) != lastpos)
			ret = FindCirclesRec(image, x+1, y+1,P(x,y),targetpos,distance +1);
		else if(P7 == 0xFF && P6 == 0 && P8 == 0 && P(x-1,y+1) != lastpos)
			ret = FindCirclesRec(image, x-1, y+1,P(x,y),targetpos,distance +1);
		else //if(P9 == 0xFF && P8 == 0 && P2 == 0 && P(x-1,y-1) != lastpos)
			ret = FindCirclesRec(image, x-1, y-1,P(x,y),targetpos,distance +1);
	}
	//next minutia reached may be of length 15
	else if (sum > 510)
	{
		*targetpos = P(x,y);
		return distance;
	}
  else
	{
		return -1;
	}
	return ret;
}

//taken from matching.c by petertu and adapted for angles from -M_PI/2 to M_PI/2
static inline FvsFloat_t anglediff(FvsFloat_t a, FvsFloat_t b)
{
  FvsFloat_t dif;

  dif = a-b;

  if (dif > -M_PI/2 && dif <= M_PI/2)
    return dif;
  else if (dif <= -M_PI/2)
    return (M_PI + dif);
  else
    return (M_PI - dif);
}

// written by petertu
/* this function removes spurs, circles and connections between parallel ridges, */
/* which are rather uncommon */
FvsError_t ImageEnhanceThinned(FvsImage_t image,FvsFloat_t* direction)
{
    FvsInt_t w       = ImageGetWidth(image);
    FvsInt_t h       = ImageGetHeight(image);
    FvsInt_t pitch   = ImageGetPitch(image);
    FvsByte_t* p     = ImageGetBuffer(image);
    FvsFloat_t* dir  = FloatFieldGetBuffer(direction);
    FvsFloat_t diffangle;
    FvsInt_t x, y, sum,targetpos;
		FvsInt_t i,j,ret,bifurcation,sumdist[8],sumtarget[8],sumstart[8];


   	//First remove spurs with length of up to 15 or isolated lines with length of up to 20
  	for (y=1; y<h-1; y++)
	  for (x=1; x<w-1; x++)
	  {
      if( P1==0xFF)
      {
      	sum = P2+P3+P4+P5+P6+P7+P8+P9;
      	//isolated pixel
      	if(sum == 0) {
      		P1 = 0;
      		continue;
      	}
      	//starting point
      	if(sum == 255) {
      		RemoveSpursRec(image,x,y,-1, 0);
				}
      }
	  }

   	//Then remocve cycles
  	for (y=1; y<h-1; y++)
	  for (x=1; x<w-1; x++)
	  {
      if( P1==0xFF)
      {
      	sum = P2+P3+P4+P5+P6+P7+P8+P9;
      	//bisection point
      	//look in every direction to find another bifurcation
	    	if(sum > 510 && !((P2 && P3) || (P3 && P4) || (P4 && P5) || (P5 && P6) || (P6 && P7) || (P7 && P8) || (P8 && P9) || (P9 && P2)))
	    	{
//	      	RemoveSpursRec(image,x,y,-1, 0);
//	      	fprintf(stdout, "%d,%d\n",x,y);
					bifurcation=0;

					if(P2 == 0xFF)
					{
						ret = FindCirclesRec(image, x, y-1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x  ,y-1); bifurcation++;}
					}
					if(P3 == 0xFF)
					{
						ret = FindCirclesRec(image, x+1, y-1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x+1,y-1); bifurcation++;}
					}
					if(P4 == 0xFF)
					{
						ret = FindCirclesRec(image, x+1, y,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x+1,y  ); bifurcation++;}
					}
					if(P5 == 0xFF)
					{
						ret = FindCirclesRec(image, x+1, y+1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x+1,y+1); bifurcation++;}
					}
					if(P6 == 0xFF)
					{
						ret = FindCirclesRec(image, x, y+1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x  ,y+1); bifurcation++;}
					}
					if(P7 == 0xFF)
					{
						ret = FindCirclesRec(image, x-1, y+1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x-1,y+1); bifurcation++;}
					}
					if(P8 == 0xFF)
					{
						ret = FindCirclesRec(image, x-1, y,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x-1,y  ); bifurcation++;}
					}
					if(P9 == 0xFF)
					{
						ret = FindCirclesRec(image, x-1, y-1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x-1,y-1); bifurcation++;}
					}

//					fprintf(stdout, "%d,%d\n",x,y);
					//now look if a circle was found
					for(i=0;i<bifurcation;i++)
					for(j=i+1;j<bifurcation;j++)
					{
						if(sumdist[i] > 0 && sumdist[j] > 0)
						{
							if(sumtarget[i] == sumtarget[j])
                            {
								if(sumdist[i] > sumdist[j])
								{
									//remove line i
									RemoveSpursRec(image,sumstart[i]%pitch,(FvsInt_t)(sumstart[i]/pitch),P(x,y),0);
								}
								else
								{
									//remove line j
									RemoveSpursRec(image,sumstart[j]%pitch,(FvsInt_t)(sumstart[j]/pitch),P(x,y),0);
								}
                            }
						}
					}
				}
      }
	  }

	  //finally remove connections between parrallel ridges (very similar algorithm to circle removement)
  	for (y=1; y<h-1; y++)
	  for (x=1; x<w-1; x++)
		{
      if( P1==0xFF)
      {
      	sum = P2+P3+P4+P5+P6+P7+P8+P9;
      	//bisection point
      	//look in every direction to find another bifurcation
	    	if(sum > 510 && !((P2 && P3) || (P3 && P4) || (P4 && P5) || (P5 && P6) || (P6 && P7) || (P7 && P8) || (P8 && P9) || (P9 && P2)))
	    	{
					bifurcation=0;

					if(P2 == 0xFF)
					{
						ret = FindCirclesRec(image, x, y-1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x  ,y-1); bifurcation++;}
					}
					if(P3 == 0xFF)
					{
						ret = FindCirclesRec(image, x+1, y-1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x+1,y-1); bifurcation++;}
					}
					if(P4 == 0xFF)
					{
						ret = FindCirclesRec(image, x+1, y,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x+1,y  ); bifurcation++;}
					}
					if(P5 == 0xFF)
					{
						ret = FindCirclesRec(image, x+1, y+1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x+1,y+1); bifurcation++;}
					}
					if(P6 == 0xFF)
					{
						ret = FindCirclesRec(image, x, y+1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x  ,y+1); bifurcation++;}
					}
					if(P7 == 0xFF)
					{
						ret = FindCirclesRec(image, x-1, y+1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x-1,y+1); bifurcation++;}
					}
					if(P8 == 0xFF)
					{
						ret = FindCirclesRec(image, x-1, y,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x-1,y  ); bifurcation++;}
					}
					if(P9 == 0xFF)
					{
						ret = FindCirclesRec(image, x-1, y-1,P(x,y),&targetpos,1);
						if(ret != -1)
							{sumdist[bifurcation] = ret; sumtarget[bifurcation] = targetpos; sumstart[bifurcation] = P(x-1,y-1); bifurcation++;}
					}

					//fprintf(stdout, "%d,%d\n",x,y);
					//now look if a normal line was found
					for(i=0;i<bifurcation;i++)
					{
						diffangle = atan2(y-sumtarget[i]/pitch,x-sumtarget[i]%pitch);
						// x < M_PI/8 because in direction matrix the directions are stored as normals to the ridge directions
						if(anglediff(dir[P(x,y)],diffangle) < M_PI/8)
						{
							RemoveSpursRec(image,sumstart[i]%pitch,(FvsInt_t)(sumstart[i]/pitch),P(x,y),0);
							break;
						}
					}
				}
      }
	  }

	return FvsOK;
}
