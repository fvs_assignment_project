/*########################################################################
*
*  Copyright(C) 2002-2012. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*                       Peter Tummeltshammer(petertu)
*           Matthias Wenzl (mw)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*  Changes: petertu -> Modified import.c and export.c
*                                   -> added function ImageEnhanceThinned()
*  Changes: mw -> Changed deprecated and adapted for dvp lu
*
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
########################################################################*/

#include "fvs.h"

int main(int argc, char *argv[])
{
    /* check if all parameters are there */
    if (argc < 3)
    {
        printf("Usage:\n %s base_image input_images...\n", argv[0]);
        return -1;
    }

    int numberOfFiles = argc - 1;
    char **listOfFiles = argv + 1;
    FvsFloat_t *goodness = calculateGoodnesses(numberOfFiles, (const char**)listOfFiles);

    for (int i = 1; i < numberOfFiles; ++i)
    {
        fprintf(stdout, "Matching: %s %s = %3.2f\n", listOfFiles[0], listOfFiles[i], goodness[i]*100);
    }

    free(goodness);
    return 0;
}
