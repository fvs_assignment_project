/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
* ########################################################################*/

#if !defined FVS__GLOBAL_HEADER__INCLUDED__
#define FVS__GLOBAL_HEADER__INCLUDED__

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*! \mainpage Fingerprint Verification System - FVS

  \section intro   Introduction
  The library provides a framework to use when creating a fingerprint
  recognition program. It provides easy to use interfaces to load
  fingerprint files, process fingerprint images and analyse the data.

  \section install Installation
  See the file INSTALL provided with the source package

*/

/*
** This is the header to include in your main program to be able to
** use the library capabilities.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/* basic type definitions */
#include "fvstypes.h"

/* file I/O */
#include "import.h"   /* automatic detection of format to import */
#include "export.h"   /* export to a file */

/* image object */
#include "image.h"

/* floating point field object */
#include "floatfield.h"

/* minutia object */
#include "minutia.h"


/* image processing functions */
#include "imagemanip.h"

/* matching algorithms */
#include "matching.h"

/*!
  Get a string that contains the version information.
  The string is in the form "1.2.4" for version 1.2, build 4.
*/
FvsString_t FvsGetVersion(void);


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif


#endif /* FVS__GLOBAL_HEADER__INCLUDED__*/
