/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
* ########################################################################*/

#if !defined FVS__MATCHING_HEADER__INCLUDED__
#define FVS__MATCHING_HEADER__INCLUDED__

#include "image.h"
#include "minutia.h"

/*!
  \todo Document and implement, remove local functions
  \param image1    First image to compare
  \param image2    Second image that wil be compared to image1
  \param pgoodness An index that indicates how good the match is, the higher, the better
  \return          An error code
*/

#define NEIGHBORS 6
#define MAXMINUTIA 100

#define DELTA_ANGLE      (4*M_PI)/180
#define DELTA_DISTANCE   1
#define DELTA_VARPHI     (1*M_PI)/180
#define DELTA_VARTHETA   (2*M_PI)/180

#define THRESHOLD_MARKED 3
#define THRESHOLD_MATCH  0.15

typedef struct Fvs_LocalStructure_t
{
  FvsFloat_t distance[NEIGHBORS];
  FvsFloat_t varphi[NEIGHBORS];
  FvsFloat_t vartheta[NEIGHBORS];
  FvsFloat_t angle;
} Fvs_LocalStructure_t;

FvsError_t MinutiaSetSort(FvsMinutia_t *src, FvsInt_t length);

FvsError_t MatchingCompareImages(const FvsImage_t image1,
                                 const FvsImage_t image2,
                                 FvsFloat_t* pgoodness);

FvsError_t MatchingCompareMinutiaSets(const FvsMinutiaSet_t set1,
    								  const FvsMinutiaSet_t set2,
    								  FvsFloat_t* pgoodness
);
/*!
  \todo Document and implement, remove local functions
  \param minutia1  First minutia set to compare
  \param minutia2  Second minutia set that wil be compared to image1
  \param pgoodness An index that indicates how good the match is, the higher, the better
  \return          An error code
*/
FvsError_t MatchingCompareMinutiaSets(const FvsMinutiaSet_t minutia1,
                                      const FvsMinutiaSet_t minutia2,
                                      FvsFloat_t* pgoodness);


#endif /* __MATCHING_HEADER__INCLUDED__ */
