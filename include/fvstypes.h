/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
* ########################################################################*/

#if !defined FVS__FVSTYPES_HEADER__INCLUDED__
#define FVS__FVSTYPES_HEADER__INCLUDED__


/* Some of the following types may already be defines in stddef.h on some
** systems. The code here will certainly be improved here (when we will
** provide a configure script). The typedefs provided here are only valid
** on some systems. Modify them according to your systems till we provide
** sufficient self detection.
*/

//#if defined(HAVE_STDINT_H) || defined(HAVE_INTTYPES_H)

//    #if defined(HAVE_STDINT_H)
        #include <stdint.h>
//    #endif

//    #if defined(HAVE_INTTYPES_H)
        #include <inttypes.h>
//    #endif

//#else
//
//    /* for windows users? */
//    typedef unsigned char  uint8_t;
//    typedef unsigned short uint16_t;
//    typedef unsigned int   uint32_t;
//
//    typedef signed char    int8_t;
//    typedef signed short   int16_t;
//    typedef signed int     int32_t;
//
//#endif


/*!
  Natural signed integer type.
*/
typedef int            FvsInt_t;


/*!
  Natural unsigned integer type.
*/
typedef unsigned int   FvsUint_t;


/*!
  Signed byte, word and dword..
*/
typedef int8_t         FvsInt8_t;
typedef int16_t        FvsInt16_t;
typedef int32_t        FvsInt32_t;


/*!
  Unsigned byte, word and dword..
*/
typedef uint8_t        FvsUint8_t;
typedef uint16_t       FvsUint16_t;
typedef uint32_t       FvsUint32_t;

typedef uint8_t        FvsByte_t;
typedef uint16_t       FvsWord_t;
typedef uint32_t       FvsDword_t;


/*!
  Floating point type.
*/
typedef float         FvsFloat_t;


/*!
  Pointer type.
*/
typedef void*          FvsPointer_t;


/*!
  Handle type to manipulate opaque structures.
*/
typedef void*          FvsHandle_t;


/*!
  String type.
*/
typedef char*          FvsString_t;


/*!
  A boolean type
*/
typedef enum FvsBool_t
{
    /*! false tag */
    FvsFalse = 0,
    /*! true tag  */
    FvsTrue  = 1
} FvsBool_t;


/*!
  An approximation of PI for systems that do not already define it.
*/

#ifndef M_PI
#define M_PI          3.1415926535897932384626433832795
#endif


/*!
  Error codes. An error code should be returned by each function from the
  interface. The user of the API should always know when an operation did
  not end up successfully.
*/
typedef enum FvsError_t
{
    /*! an undefined error, use with parcimony */
    FvsFailure          = -1,
    /*! no error */
    FvsOK               = 0,
    /*! not enough memory */
    FvsMemory,
    /*! an invalid parameter was used */
    FvsBadParameter,
    /*! bad file format */
    FvsBadFormat,
    /*! input / output error */
    FvsIoError,
} FvsError_t;


#endif /* FVS__FVSTYPES_HEADER__INCLUDED__ */

