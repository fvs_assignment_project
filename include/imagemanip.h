/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
* ########################################################################*/

#if !defined FVS__IMAGEMANIP_HEADER__INCLUDED__
#define FVS__IMAGEMANIP_HEADER__INCLUDED__

/* include basic image manipulation functions */
#include "img_base.h"
#include "floatfield.h"


/*!
  Get a floating point field that contains the ridge direction of the
  fingerprint image.
  The algorithm used here is described in many scientific papers. The
  result is better if the image has a high contrast and is normalized.
  The values representing the direction vary between -PI/2 and PI/2
  radians the ridge direction is not oriented.
  The higher the block size, the better is the analysis but it will
  take even longer to perform the computations. The low pass filter
  size allows to filter out errors as the ridge direction varies slowly
  in the fingerprint.
  \param image       Image to extract the ridge direction from
  \param field       Ridge directions (floating point values)
  \param nBlockSize  Size of the blocks into the image is divided into
  \param nFilterSize Size of the low pass filter (set to 0 to deactivate)
  \return        An error code.
*/
FvsError_t FingerprintGetDirection(const FvsImage_t image, FvsFloatField_t field,
            const FvsInt_t nBlockSize, const FvsInt_t nFilterSize);

/*!
  Get a floating point field that contains the ridge frequency.
  \param image       Image to extract the ridge frequency direction from
  \param direction   Ridge directions (floating point values)
  \param frequency   Ridge frequency
  \return        An error code.
*/
FvsError_t FingerprintGetFrequency(const FvsImage_t image, const FvsFloatField_t direction,
            FvsFloatField_t frequency);

/*!
  Get a mask that tells where are the parts of the fingerprint that contain
  useful information for further processing. The mask should be set to 0 where
  the information is not usable: on borders, background parts, where the
  fingerprint image is of very bad quality, ... The mask is set to 255 where
  the fingerprint data can be used.
  \param image       Image to extract the ridge frequency direction from
  \param direction   Ridge directions (floating point values)
  \param frequency   Ridge frequency
  \param mask        The output mask
  \return        An error code.
*/
FvsError_t FingerprintGetMask(const FvsImage_t image, const FvsFloatField_t frequency, FvsImage_t mask);

/* generates a fixed size mask with a fixed size border */
FvsError_t FingerprintGetFixedMask(const FvsImage_t image, FvsImage_t mask, const FvsInt_t border );

/*!
  Thin the image using connectivity information.
  The image must be binarized (i.e. contain only 0x00 or 0xFF values)
  This algorithm looks at the immediate neighbourhood of a pixel and
  checks how many transitions from black to white are present. According
  to the result, the pixel is removed or not.
  \param image   A binarized image to modify
  \return        An error code.
*/
FvsError_t ImageThinConnectivity(FvsImage_t image);


/*!
  Thin the image using "Hit and Miss" structuring elements.
  The image must be binarized (i.e. contain only 0x00 or 0xFF values)
  This algo has the disadvantage to produce spurious lines resulting
  from the skeletonization. These may be eliminated by another algorithm.
  postprocessing is then still needed afterwards.
  \param image   A binarized image to modify
  \return        An error code.
*/
FvsError_t ImageThinHitMiss(FvsImage_t image);


/*!
  Local contrast stretching operator.
  \param image     Image to stretch
  \param size      Size of the blocks onto which stretching should be applied
  \param tolerance value used to eliminate the border of the local histogram
  \return          An error code
*/
FvsError_t ImageLocalStretch(FvsImage_t image, const FvsInt_t size, const FvsInt_t tolerance);






/* The functions below are implemented in imageenhance.c 														*/


/* this function removes spurs, circles and connections between parallel ridges, */
/* which are rather uncommon */
FvsError_t ImageEnhanceThinned(FvsImage_t image,FvsFloat_t* direction);

/*!
  Enhance a fingerprint image.
  The algorithm used here is complex to describe but the postprocessing is
  based on Gabor filters whose parameters are computed dynamically.
  The image is change in-place so you will have to make a copy to preserve the
  original image.
  \param image     Image to modify
  \param direction A ridge direction field previously computed with FingerprintGetDirection for example
  \param frequency A ridge frequency field previously computed with FingerprintGetFrequency for example
  \param mask      Indicating which parts of the image are relevant
  \param radius    Action radius of the filter. 4.0 should be OK in most cases. The higher this value,
                   the resistenter the algorithm is to noise but then it produces more spurious lines.
  \return          An error code.
*/
FvsError_t ImageEnhanceGabor(FvsImage_t image, const FvsFloatField_t direction,
             const FvsFloatField_t frequency, const FvsImage_t mask, const FvsFloat_t radius);


#endif /* FVS__IMAGEMANIP_HEADER__INCLUDED__ */
