/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
* ########################################################################*/

#if !defined FVS__IMAGE_BASE_HEADER__INCLUDED__
#define FVS__IMAGE_BASE_HEADER__INCLUDED__

#include "image.h"

typedef enum FvsLogical_t
{
    FvsLogicalOr   = 1,
    FvsLogicalAnd  = 2,
    FvsLogicalXor  = 3,
    FvsLogicalNAnd = 4,
    FvsLogicalNOr  = 5,
    FvsLogicalNXor = 6,
} FvsLogical_t;

/*!
  Transform a gray image into a binary image with either the value 0
  or 255 as pixel color.
  \param image   Image to binarize
  \param limit   Threshold value
  \return        An error code
*/
FvsError_t ImageBinarize(FvsImage_t image, const FvsByte_t limit);


/*!
  Inverts the pixel colors of an image.
  \param image   Image to binarize
  \return        An error code
*/
FvsError_t ImageInvert(FvsImage_t image);


/*!
  Compute the average of 2 images overwrites the result in the first image.
  \param image1  Image that will contain the result
  \param image2  Image to combine image1 with
  \return        An error code
*/
FvsError_t ImageAverage(FvsImage_t image1, const FvsImage_t image2);


/*!
  Compute the average of 2 images overwrites the result in the first image.
  This function makes a circular average computation. 0 and 255 will result
  in 0 as an average instead of 127 as with the precedent function.
  \param image1  Image that will contain the result
  \param image2  Image to combine image1 with
  \return        An error code
*/
FvsError_t ImageAverageModulo(FvsImage_t image1, const FvsImage_t image2);


/*!
  Compute a logical combination of two images.
  \param image1  Image that will contain the result
  \param image2  Image to combine image1 with
  \return        An error code
*/
FvsError_t ImageLogical(FvsImage_t image1,  const FvsImage_t image2,  const FvsLogical_t operation);


/*!
  Translate an image given a vector.
  \todo Implement the function
  \param image   Image to translate
  \param vx      x vector
  \param vy      y vector
  \return        An error code
*/
FvsError_t ImageTranslate(FvsImage_t image, const FvsInt_t vx, const FvsInt_t vy);


/*!
  Create a test image composed of stripes.
  \param image      Image to modify
  \param horizontal horizontal or vertical stripes
  \return           An error code
*/
FvsError_t ImageStripes(FvsImage_t image, const FvsBool_t horizontal);


/*!
  Normalize an image pixel-wise so that it gets a maximum local variance
  \param image      Image to modify
  \param size       Size of the area considered around each pixel 
  									for calculating the variance
  \return           An error code
*/
FvsError_t ImagePixelNormalize(FvsImage_t image, const FvsInt_t size);


/*!
  Change the luminosity of an image argument ranging [-255..255] 
  \param image      Image to modify
  \param luminosity Relative luminosity factor
  \return           An error code
*/
FvsError_t ImageLuminosity(FvsImage_t image, const FvsInt_t luminosity);


/*!
  Change the contrast of an image argument ranging [-127..127]
  \param image      Image to modify
  \param contrast   Relative contrast factor
  \return           An error code
*/
FvsError_t ImageContrast(FvsImage_t image, const FvsInt_t contrast);


/*!
  Soften the image by computing a mean value.
  \param image      Image to modify
  \param size       Size of the computation window
  \return           An error code
*/
FvsError_t ImageSoftenMean(FvsImage_t image, const FvsInt_t size);

/*!
  Use a structural operator to dilate the binary image.
  \param image    image to dilate
  \return An error code
*/
FvsError_t ImageDilate(FvsImage_t image);


/*!
  Use a structural operator to erode the binary image.
  \param image    image to dilate
  \return An error code
*/
FvsError_t ImageErode(FvsImage_t image);

#endif /* FVS__IMAGE_BASE_HEADER__INCLUDED__ */


