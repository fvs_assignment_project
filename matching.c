/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
########################################################################*/
// this code by pallav gupta (pallav@ieee.org)

// this algorithm is described in two papers.

// xudong jiang and wei-yun yau, "fingerprint minutiae matching based on the
//local and global structure

// shenglin yang and ingrid m. verbauwhede, "a secure fingerprint matching
// technique"

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "matching.h"

static FvsInt_t CompareLocalStructures(Fvs_LocalStructure_t *ils,
                                       Fvs_LocalStructure_t *tls,
                                       FvsInt_t ilen, FvsInt_t tlen);

static void FindMaxDistance(FvsMinutia_t *src, FvsInt_t *neigh, FvsInt_t ref,
                            FvsFloat_t *d, FvsInt_t *idx);
static void FindNeighbors(FvsMinutia_t *src, FvsInt_t length, FvsInt_t ineigh[][NEIGHBORS]);
static void ComputeLocalStructure(FvsMinutia_t *src, FvsInt_t length,
                                  FvsInt_t ineigh[][NEIGHBORS],
                                  Fvs_LocalStructure_t *ls);
static inline FvsFloat_t anglediff(FvsFloat_t a, FvsFloat_t b);


FvsError_t MatchingCompareMinutiaSets
(
    const FvsMinutiaSet_t set1,
    const FvsMinutiaSet_t set2,
    FvsFloat_t* pgoodness
)
{
  FvsMinutia_t *iminu = MinutiaSetGetBuffer(set1);
  FvsInt_t nb_iminu = MinutiaSetGetCount(set1);

  FvsMinutia_t *tminu = MinutiaSetGetBuffer(set2);
  FvsInt_t nb_tminu = MinutiaSetGetCount(set2);

  FvsInt_t ineigh[MAXMINUTIA][NEIGHBORS] = {};
  FvsInt_t tneigh[MAXMINUTIA][NEIGHBORS] = {};

  Fvs_LocalStructure_t ls_iminu[MAXMINUTIA];
  Fvs_LocalStructure_t ls_tminu[MAXMINUTIA];

  FvsInt_t matched, max;

  // find the neigherest neighbors
  FindNeighbors(iminu, nb_iminu, ineigh);
  FindNeighbors(tminu, nb_tminu, tneigh);
  // compute the local structures of the minutia
  ComputeLocalStructure(iminu, nb_iminu, ineigh, ls_iminu);
  ComputeLocalStructure(tminu, nb_tminu, tneigh, ls_tminu);
  matched = CompareLocalStructures(ls_iminu, ls_tminu, nb_iminu, nb_tminu);
  max = (nb_iminu > nb_tminu) ? nb_iminu : nb_tminu;
  (*pgoodness) = (FvsFloat_t)matched/max;
  return FvsOK;
}


static FvsInt_t CompareLocalStructures(Fvs_LocalStructure_t *ils,
				       Fvs_LocalStructure_t *tls,
				       FvsInt_t ilen, FvsInt_t tlen)
{
  Fvs_LocalStructure_t s1, s2;
  FvsInt_t marked, matched;
  FvsInt_t i, j, k, l;

  matched = 0;
  marked = 0;

  // one minutia from template
  for (i = 0; i < ilen; i++) {
    s1 = ils[i];

    // one minutia from input
    for (j = 0; j < tlen; j++) {

      s2 = tls[j];

      // need to go further only if the local ridge direction are similiar
      if (fabs(s1.angle - s2.angle) > DELTA_ANGLE)
				continue;

      marked = 0;

      // each neighbor of input minutia
      for (k = 0; k < NEIGHBORS; k++) {
				// each neighbor of template minutia
				for (l = 0; l < NEIGHBORS; l++) {

				  if (fabs(s1.distance[k] - s2.distance[l]) > DELTA_DISTANCE)
				    continue;
				  if (fabs(s1.varphi[k] - s2.varphi[l]) > DELTA_VARPHI)
				    continue;
				  if (fabs(s1.vartheta[k] - s2.vartheta[l]) > DELTA_VARTHETA)
				    continue;
				  // all conditions matched, consider k'th neighboer and l'th neighbor marked
				  marked++;
				}
      }

      if (marked > THRESHOLD_MARKED)
				matched++;
    }
  }

  return matched;
}


static void ComputeLocalStructure(FvsMinutia_t *src, FvsInt_t length,
				  FvsInt_t ineigh[][NEIGHBORS], Fvs_LocalStructure_t *ls)
{
  FvsFloat_t deltax, deltay, distance;
  FvsInt_t i, j, p;

  for (i = 0; i < length; i++) {
    for (j = 0; j < NEIGHBORS; j++) {
      // get index of jth neighbor
      p = ineigh[i][j];

      // compute distance
      deltax = src[p].x - src[i].x;
      deltay = src[p].y - src[i].y;
      distance = sqrt(deltax*deltax + deltay*deltay);
      ls[i].distance[j] = distance;

      // compute phi
      ls[i].varphi[j] = anglediff(src[p].angle, src[i].angle);

      // compute vartheta
      ls[i].vartheta[j] = anglediff(atan2(deltay, deltax), src[i].angle);

      // assign angle
      ls[i].angle = src[i].angle;
    }
  }
}

static inline FvsFloat_t anglediff(FvsFloat_t a, FvsFloat_t b)
{
  FvsFloat_t dif;

  dif = a-b;

  if (dif > -M_PI && dif <= M_PI)
    return dif;
  else if (dif <= -M_PI)
    return (2*M_PI + dif);
  else
    return (2*M_PI - dif);
}



static void FindNeighbors(FvsMinutia_t *src, FvsInt_t length, FvsInt_t ineigh[][NEIGHBORS])
{

  FvsInt_t i, j, k, idx, end, wrap = 0;
  FvsFloat_t deltax, deltay, distance, maxdis;

  // find the neigherest neighbors for input minutia
  for (i = 0; i < length; i++) {
    // pick the first 6 points, aribtrarily
    j = i+1;
    for (k = 0; k < NEIGHBORS; k++) {
      if (j >= length) {
				j = 0;
				wrap = 1;
      }
      ineigh[i][k] = j++;
    }

    // find the max distance
    FindMaxDistance(src, ineigh[i], i, &maxdis, &idx);

    end = wrap ? i : length;
    j = wrap ? i-1 : j;
    // search upwards
    while (((src[j].x - src[i].x) < maxdis) && (j < end) && (j >= 0)) {
      deltax = src[j].x - src[i].x;
      deltay = src[j].y - src[i].y;
      distance = sqrt(deltax*deltax + deltay*deltay);

      if (distance < maxdis) {
				ineigh[i][idx] = j;
				FindMaxDistance(src, ineigh[i], i, &maxdis, &idx);
      }

      if (wrap)
				j--;
      else
				j++;
    }

    // only need to search backwards if when picking we didn't wrap around
    // if we wrapped around searching upwards will have covered all points
    if (!wrap) {
      j = i-1;
      while (((src[i].x - src[j].x) < maxdis) && (j >= 0)) {
				deltax = src[i].x - src[j].x;
				deltay = src[i].y - src[j].y;
				distance = sqrt(deltax*deltax + deltay*deltay);

				if (distance < maxdis) {
				  ineigh[i][idx] = j;
				  FindMaxDistance(src, ineigh[i], i, &maxdis, &idx);
				}
				j--;
      }
    }
  }
}

static void FindMaxDistance(FvsMinutia_t *src, FvsInt_t *neigh, FvsInt_t ref, FvsFloat_t *d,
			    FvsInt_t *idx)
{
  FvsInt_t i, maxi = 0, p = neigh[0];
  FvsFloat_t deltax = src[p].x - src[ref].x;
  FvsFloat_t deltay = src[p].y - src[ref].y;
  FvsFloat_t maxd = sqrt(deltax*deltax + deltay*deltay), tempd;

  //  printf("origin: %f %f\n", src[ref].x, src[ref].y);
  //printf("0: %f %f %f\n", src[p].x, src[p].y, tempd);

  for (i = 1; i < NEIGHBORS; i++) {
    p = neigh[i];
    deltax = src[p].x - src[ref].x;
    deltay = src[p].y - src[ref].y;
    tempd = sqrt(deltax*deltax + deltay*deltay);
    //printf("%d: %f %f %f\n", i, src[p].x, src[p].y, tempd);

    if (tempd > maxd) {
      maxd = tempd;
      maxi = i;
    }
  }

  //printf("max = %f, idx = %d\n\n\n", maxd, maxi);
  (*d) = maxd;
  (*idx) = maxi;
}

FvsError_t MinutiaSetSort(FvsMinutia_t *src, FvsInt_t length)
{
  FvsFloat_t x, y, angle;
  FvsMinutiaType_t type;

  FvsInt_t i, j, looking;

  if (src == NULL)
    return FvsMemory;

  for (i = 1; i < length; i++) {

    x = src[i].x;
    y = src[i].y;
    angle = src[i].angle;
    type = src[i].type;

    j = i-1;
    looking = 1;

    while ((j >= 0) && (looking == 1)) {
      // sort with respec to x coordinate
      if (x < src[j].x) {
				src[j + 1].angle = src[j].angle;
				src[j + 1].x = src[j].x;
				src[j + 1].y = src[j].y;
				src[j + 1].type = src[j].type;

				src[j].angle = angle;
				src[j].x = x;
				src[j].y = y;
				src[j].type = type;

				--j;
			 } else  {
				looking = 0;
				src[j + 1].angle = angle;
				src[j + 1].x = x;
				src[j + 1].y = y;
				src[j + 1].type = type;
      }
    }
  }

  return FvsOK;
}
