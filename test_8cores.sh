#!/bin/sh

set -eu
IFS='\n\t'

make test

echo 'Using all cores (default OpenMP config)'
time ./bin/fvs_test images/*
echo

echo 'Using 4 cores'
export OMP_NUM_THREADS=4
time ./bin/fvs_test images/*
echo

echo 'Using 2 cores'
export OMP_NUM_THREADS=2
time ./bin/fvs_test images/*
echo

echo 'Using 1 core'
export OMP_NUM_THREADS=1
time ./bin/fvs_test images/*
