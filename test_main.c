#include "fvs.h"
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
/* For doing time calculations */
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

static float subtractTimeval(struct timeval value1, struct timeval value2)
{
    long time1 = value1.tv_sec * 1000000 + value1.tv_usec;
    long time2 = value2.tv_sec * 1000000 + value2.tv_usec;
    return (float)(time1 - time2) / 1000000;
}

static float subtractTimeSpec(struct timespec value1, struct timespec value2)
{
    long time1 = value1.tv_sec * 1000000000 + value1.tv_nsec;
    long time2 = value2.tv_sec * 1000000000 + value2.tv_nsec;
    return (float)(time1 - time2) / 1000000000;
}

/* reverse 'arr', which has 'narr' elements */
static void reverse(FvsMinutiaSet_t *arr, size_t narr)
{
    size_t i;

    for (i=0; i < narr / 2; ++i) {
        FvsMinutiaSet_t *tmp = arr[i];
        arr[i] = arr[narr-i-1];
        arr[narr-i-1] = tmp;
    }
}

/* rotate 'arr' of size 'narr' by 'shift' */
static void rotate(FvsMinutiaSet_t *arr, size_t narr, unsigned long shift)
{
    reverse(arr, shift);
    reverse(arr + shift, narr - shift);
    reverse(arr, narr);
}

static int compareFloats(float a, float b)
{
    float diff = fmaxf(a, b) - fminf(a, b);
    if (diff < 0.015) {
        return 1;
    }
    return 0;
}

int main(int argc, const char *argv[])
{
    int numberOfFiles = argc - 1;
    const char **testFiles = argv + 1;
    float *expectedResults = (float*)malloc(numberOfFiles * sizeof(float));
    struct timespec time1, time2;
    struct rusage usage1, usage2;
    float elaspedTime, cpuTime, averageWallTime;
    float *results;

    /* check if all parameters are there */
    if (argc < 3)
    {
        printf("Usage:\n %s image1 image2...\n", argv[0]);
        return -1;
    }
    memset(expectedResults, 0, numberOfFiles);
    expectedResults[0] = 1;
    FvsMinutiaSet_t *minutiaArray = loadMinutiaSetsFromFiles(numberOfFiles, testFiles);

    getrusage(RUSAGE_SELF, &usage1);
    clock_gettime(CLOCK_MONOTONIC_RAW, &time1);

    for (int j = 0; j < numberOfFiles; ++j)
    {
        results  = compareMinutiaSets(numberOfFiles, minutiaArray);
        for (int i = 0; i < numberOfFiles; ++i)
            if (!compareFloats(expectedResults[i], results[i]))
                printf("ERROR: %d %d %s %s %.2f %.2f\n", j, i, testFiles [0], testFiles[i], expectedResults[i], results[i]);
        free(results);
        rotate(minutiaArray, numberOfFiles, 1);
    }

    getrusage(RUSAGE_SELF, &usage2);
    clock_gettime(CLOCK_MONOTONIC_RAW, &time2);

    elaspedTime = subtractTimeSpec(time2, time1);
    averageWallTime = elaspedTime / numberOfFiles;
    cpuTime = subtractTimeval(usage2.ru_utime, usage1.ru_utime);
    printf("Compare %d files : %02.3fsec = %02.3fsec/file, cpu time: %02.3fsec, speedup: %02.2fx\n",
           numberOfFiles, elaspedTime, averageWallTime, cpuTime, cpuTime / elaspedTime);

    cleanupMinutiaSetArray(numberOfFiles, minutiaArray);
    free(expectedResults);
    return 0;
}
