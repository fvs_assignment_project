/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
########################################################################*/

#include "fvs.h"
#include "img_base.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>


/* Transform the gray image into a black & white binary image */
FvsError_t ImageBinarize(FvsImage_t image, const FvsByte_t limit)
{
    FvsInt_t n;
    FvsByte_t *pimg = ImageGetBuffer(image);
    FvsInt_t size = ImageGetSize(image);
    if (pimg==NULL)
        return FvsMemory;
    /* loop through each pixel */
    for (n = 0; n < size; n++, pimg++)
    {
        /* now a do some math to decided if its white or black */
        *pimg = (*pimg < limit)?(FvsByte_t)0xFF:(FvsByte_t)0x00;
    }
    return ImageSetFlag(image, FvsImageBinarized);
}


/* Invert image pixel values */
FvsError_t ImageInvert(FvsImage_t image)
{
    FvsByte_t* pimg = ImageGetBuffer(image);
    FvsInt_t size = ImageGetSize(image);
    FvsInt_t n;
    if (pimg==NULL)
        return FvsMemory;
    for (n = 0; n < size; n++, pimg++)
    {
        *pimg = 0xFF - *pimg;
    }
    return FvsOK;
}


/* compute the average of 2 images */
FvsError_t ImageAverage(FvsImage_t image1, const FvsImage_t image2)
{
    FvsByte_t* p1 = ImageGetBuffer(image1);
    FvsByte_t* p2 = ImageGetBuffer(image2);
    FvsInt_t size1 = ImageGetSize(image1);
    FvsInt_t size2 = ImageGetSize(image2);
    FvsInt_t i;

    if (p1==NULL || p2==NULL)
        return FvsMemory;
    if (size1!=size2)
        return FvsBadParameter;

    for (i = 0; i < size1; i++, p1++)
    {
        *p1 = (*p1+*p2++)>>1;
    }
    return FvsOK;
}


/* compute a logical operation */
FvsError_t ImageLogical
    (
    FvsImage_t image1,
    const FvsImage_t image2,
    const FvsLogical_t operation
    )
{
    FvsByte_t* p1 = ImageGetBuffer(image1);
    FvsByte_t* p2 = ImageGetBuffer(image2);
    FvsInt_t size1 = ImageGetSize(image1);
    FvsInt_t i;

    if (p1==NULL || p2==NULL)
        return FvsMemory;
    if (ImageCompareSize(image1, image2)==FvsFalse)
        return FvsBadParameter;

    switch (operation)
    {
    case FvsLogicalOr:
        for (i = 0; i < size1; i++, p1++)
            *p1 = (*p1) | (*p2++);
        break;
    case FvsLogicalAnd:
        for (i = 0; i < size1; i++, p1++)
            *p1 = (*p1) & (*p2++);
        break;
    case FvsLogicalXor:
        for (i = 0; i < size1; i++, p1++)
            *p1 = (*p1) ^ (*p2++);
        break;
    case FvsLogicalNAnd:
        for (i = 0; i < size1; i++, p1++)
            *p1 = ~((*p1) & (*p2++));
        break;
    case FvsLogicalNOr:
        for (i = 0; i < size1; i++, p1++)
            *p1 = ~((*p1) | (*p2++));
        break;
    case FvsLogicalNXor:
        for (i = 0; i < size1; i++, p1++)
            *p1 = ~((*p1) ^ (*p2++));
        break;
    }
    return FvsOK;
}


/* compute the average of 2 images modulo 256 */
FvsError_t ImageAverageModulo(FvsImage_t image1, const FvsImage_t image2)
{
    FvsByte_t* p1 = ImageGetBuffer(image1);
    FvsByte_t* p2 = ImageGetBuffer(image2);
    FvsInt_t size1 = ImageGetSize(image1);
    FvsInt_t size2 = ImageGetSize(image2);
    FvsInt_t i;
    FvsByte_t v1, v2;

    if (size1!=size2)
        return FvsBadParameter;

    if (p1==NULL || p2==NULL)
        return FvsMemory;

    for (i = 0; i < size1; i++)
    {
        v1 = *p1;
        v2 = *p2;
        if (v1<128) v1+=256;
        if (v2<128) v2+=256;
        v1 += v2;
        v1 >>=1;
        v1 = v1%256;
        *p1++ = (uint8_t)v1;
    }
    return FvsOK;
}

#define P(x,y)      p[((x)+(y)*pitch)]


/* create a test image composed of stripes */
FvsError_t ImageStripes(FvsImage_t image, const FvsBool_t horizontal)
{
    FvsByte_t* p = ImageGetBuffer(image);
    FvsInt_t w     = ImageGetWidth (image);
    FvsInt_t h     = ImageGetHeight(image);
    FvsInt_t pitch = ImageGetPitch (image);
    FvsInt_t x,y;
    if (p==NULL)
        return FvsMemory;
    if (horizontal==FvsFalse)
    {
        for (y = 0; y < h; y++)
        for (x = 0; x < w; x++)
            P(x,y) = (FvsByte_t)x%256;
    }
    else
    {
        for (y = 0; y < h; y++)
        for (x = 0; x < w; x++)
            P(x,y) = (FvsByte_t)y%256;
    }
    return FvsOK;
}


/*rewritten by petertu */
/*   Normalize an image pixel-wise so that it gets a maximum local variance */
FvsError_t ImagePixelNormalize(FvsImage_t image, const FvsInt_t size)
{
    FvsByte_t* p1  = ImageGetBuffer(image);
    FvsByte_t* p2;
    FvsInt_t   w   = ImageGetWidth (image);
    FvsInt_t   h   = ImageGetHeight(image);
    FvsInt_t pitch = ImageGetPitch (image);
    FvsInt_t pitch2;
    FvsInt_t x,y,s,p,q, min, max;
    FvsFloat_t fgray;
    FvsImage_t im2;

    im2 = ImageCreate();

    if (im2==NULL || p1==NULL)
        return FvsMemory;

    s = size/2;		/* size */
    if (size<=0)
			return FvsBadParameter;

    /* copy image to make the computation */
    ImageCopy(im2, image);
    p2 = ImageGetBuffer(im2);
    if (p2==NULL)
    {
			ImageDestroy(im2);
			return FvsMemory;
    }
    pitch2 = ImageGetPitch (im2);

    for (y = 0; y < h; y++)
    for (x = 0; x < w; x++)
    {
				min=255;
				max=0;
				for (q=-s;q<=s;q++)
				for (p=-s;p<=s;p++)
				{
						if(x+p >= 0 && x+p < w && y+q >= 0 && y+q < h) {
			    	   max = p2[(x+p)+(y+q)*pitch2] > max ? p2[(x+p)+(y+q)*pitch2] : max;
			    	   min = p2[(x+p)+(y+q)*pitch2] < min ? p2[(x+p)+(y+q)*pitch2] : min;
			    	}
				}
//				fgray = (FvsFloat_t)p2[x + y*pitch2]*255.0/(FvsFloat_t)max*(1.0-(FvsFloat_t)min/255.0);
				fgray = ((FvsFloat_t)(p2[x + y*pitch2] - min))/((FvsFloat_t)(max-min)) * 255;
				if(fgray > 255.0)
					fgray = 255.0;
				if (fgray < 0)
					fgray = 0;

				p1[x + y*pitch] = fgray;
    }

    ImageDestroy(im2);
    return FvsOK;
}




/* change the luminosity of an image argument ranging [-255..255] */
FvsError_t ImageLuminosity(FvsImage_t image, const FvsInt_t luminosity)
{
    FvsByte_t* p = ImageGetBuffer(image);
    FvsInt_t  w = ImageGetWidth (image);
    FvsInt_t  h = ImageGetHeight(image);
    FvsInt_t pitch = ImageGetPitch (image);
    FvsInt_t x,y;
    FvsFloat_t fgray, a, b;
    if (p==NULL)
        return FvsMemory;
    if (luminosity>0)
    {
        a = (255.0 - abs(luminosity)) / 255.0;
        b = (FvsFloat_t)luminosity;
    }
    else
    {
        a = (255.0 - abs(luminosity)) / 255.0;
        b = 0.0;
    }
    for (y = 0; y < h; y++)
    for (x = 0; x < w; x++)
    {
        fgray = (FvsFloat_t)P(x,y);
        fgray = b + a*fgray;
        if (fgray < 0.0)    fgray = 0.0;
        if (fgray > 255.0)  fgray = 255.0;
        P(x,y)= (uint8_t)fgray;
    }
    return FvsOK;
}


/* change the contrast of an image argument ranging [-127..127] */
FvsError_t ImageContrast(FvsImage_t image, const FvsInt_t contrast)
{
    FvsByte_t* p = ImageGetBuffer(image);
    FvsInt_t  w = ImageGetWidth (image);
    FvsInt_t  h = ImageGetHeight(image);
    FvsInt_t pitch = ImageGetPitch (image);
    FvsInt_t x,y;
    FvsFloat_t fgray, a, b;
    if (p==NULL)
        return FvsMemory;
    a = (FvsFloat_t)((127.0 + contrast) / 127.0);
    b = (FvsFloat_t)(-contrast);
    for (y = 0; y < h; y++)
    for (x = 0; x < w; x++)
    {
        fgray = (FvsFloat_t)P(x,y);
        fgray = b + a*fgray;
        if (fgray < 0.0)    fgray = 0.0;
        if (fgray > 255.0)  fgray = 255.0;
        P(x,y)= (uint8_t)fgray;
    }
    return FvsOK;
}


FvsError_t ImageSoftenMean(FvsImage_t image, const FvsInt_t size)
{
    FvsByte_t* p1  = ImageGetBuffer(image);
    FvsByte_t* p2;
    FvsInt_t   w   = ImageGetWidth (image);
    FvsInt_t   h   = ImageGetHeight(image);
    FvsInt_t pitch = ImageGetPitch (image);
    FvsInt_t pitch2;
    FvsInt_t x,y,s,p,q,a,c;
    FvsImage_t im2;

    im2 = ImageCreate();

    if (im2==NULL || p1==NULL)
        return FvsMemory;

    s = size/2;		/* size */
    a = size*size;	/* area */
    if (a==0)
			return FvsBadParameter;

    /* copy image to make the computation */
    ImageCopy(im2, image);
    p2 = ImageGetBuffer(im2);
    if (p2==NULL)
    {
			ImageDestroy(im2);
			return FvsMemory;
    }
    pitch2 = ImageGetPitch (im2);

    for (y = s; y < h-s; y++)
    for (x = s; x < w-s; x++)
    {
				c = 0;
				for (q=-s;q<=s;q++)
				for (p=-s;p<=s;p++)
				{
			    	    c += p2[(x+p)+(y+q)*pitch2];
				}
        p1[x+y*pitch] = c/a;
    }

    ImageDestroy(im2);
    return FvsOK;
}




/* Use a structural operator to dilate the image
**    X
**  X X X
**    X
*/
FvsError_t ImageDilate(FvsImage_t image)
{
    FvsInt_t w      = ImageGetWidth (image);
    FvsInt_t h      = ImageGetHeight(image);
    FvsInt_t pitch  = ImageGetPitch (image);
    FvsInt_t size   = ImageGetSize  (image);
    FvsByte_t* p    = ImageGetBuffer(image);
    FvsInt_t x,y;

    if (p==NULL)
        return FvsMemory;

    for (y=1; y<h-1; y++)
    for (x=1; x<w-1; x++)
    {
        if (P(x,y)==0xFF)
        {
            P(x-1, y) |= 0x80;
            P(x+1, y) |= 0x80;
            P(x, y-1) |= 0x80;
            P(x, y+1) |= 0x80;
        }
    }

    for (y=0; y<size; y++)
        if (p[y])
            p[y] = 0xFF;

    return FvsOK;
}

FvsError_t ImageErode(FvsImage_t image)
{
    FvsInt_t w      = ImageGetWidth (image);
    FvsInt_t h      = ImageGetHeight(image);
    FvsInt_t pitch  = ImageGetPitch (image);
    FvsInt_t size   = ImageGetSize  (image);
    FvsByte_t* p    = ImageGetBuffer(image);
    FvsInt_t x,y;

    if (p==NULL)
        return FvsMemory;

    for (y=1; y<h-1; y++)
    for (x=1; x<w-1; x++)
    {
        if (P(x,y)==0x0)
        {
            P(x-1, y) &= 0x80;
            P(x+1, y) &= 0x80;
            P(x, y-1) &= 0x80;
            P(x, y+1) &= 0x80;
        }
    }

    for (y=0; y<size; y++)
        if (p[y]!=0xFF)
            p[y] = 0x0;

    return FvsOK;
}
