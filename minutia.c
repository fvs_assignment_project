/*########################################################################
*
*  Copyright(C) 2002-2007. All Rights Reserved.
*
*  Authors: Shivang Patel
*           Jaap de Haan(jdh)
*
*  Changes: jdh -> Added support for ImageMagick that enables
*                  to export files to more than 40 formats.
*   This is free software; you can redistribute it and/or modify it under
*   the terms of the GNU General Public License as published by the Free
*   Software Foundation; either version 2, or (at your option) any later
*   version.
*
*   This is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
*   for more details.
*
*   You should have received a copy of the GNU General Public License with
*   the fvs source package as the
*   file COPYING. If not, write to the Free Software Foundation, Inc.,
*   59 Temple Place - Suite 330, Boston, MA
*   02111-1307, USA.
########################################################################*/

/* For doing time calculations */
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "minutia.h"
#include "matching.h"
#include "fvs.h"


typedef struct iFvsMinutiaSet_t
{
    FvsInt_t     nbminutia; /* number of minutia set in the table */
    FvsInt_t     tablesize; /* max number of minutia that can be stored in the table */
    FvsMinutia_t ptable[1]; /* the minutia table starts here */
} iFvsMinutiaSet_t;




FvsMinutiaSet_t MinutiaSetCreate(const FvsInt_t size)
{
    iFvsMinutiaSet_t* p = NULL;
    p = (iFvsMinutiaSet_t*)malloc(sizeof(iFvsMinutiaSet_t)+size*sizeof(FvsMinutia_t));
    if (p!=NULL)
    {   /* no minutia in table yet */
        p->nbminutia = 0;
        p->tablesize = size;
    }
    return (FvsMinutiaSet_t)p;
}


void MinutiaSetDestroy(FvsMinutiaSet_t minutia)
{
    iFvsMinutiaSet_t* p = NULL;
    if (minutia==NULL)
        return;
    p = minutia;
    free(p);
}


/* returns the maximum number of minutia the table can contain */
FvsInt_t MinutiaSetGetSize(const FvsMinutiaSet_t min)
{
    const iFvsMinutiaSet_t* minutia = (iFvsMinutiaSet_t*)min;
    FvsInt_t nret = 0;

    if (minutia!=NULL)
        nret = minutia->tablesize;

    return nret;
}


/* returns the number of minutia in the set */
FvsInt_t MinutiaSetGetCount(const FvsMinutiaSet_t min)
{
    const iFvsMinutiaSet_t* minutia = (iFvsMinutiaSet_t*)min;
    FvsInt_t nret = 0;

    if (minutia!=NULL)
        nret = minutia->nbminutia;

    return nret;
}


/* returns a pointer to the table of minutia */
FvsMinutia_t* MinutiaSetGetBuffer(FvsMinutiaSet_t min)
{
    iFvsMinutiaSet_t* minutia = (iFvsMinutiaSet_t*)min;
    FvsMinutia_t* pret = NULL;

    if (minutia!=NULL)
        pret = minutia->ptable;

    return pret;
}


/* empty the minutia set */
FvsError_t MinutiaSetEmpty(FvsMinutiaSet_t min)
{
    iFvsMinutiaSet_t* minutia = (iFvsMinutiaSet_t*)min;
    FvsError_t nRet = FvsOK;

    if (minutia!=NULL)
        minutia->nbminutia = 0;
    else
        nRet = FvsMemory;

    return nRet;
}


FvsError_t MinutiaSetAdd(FvsMinutiaSet_t min,
       const FvsFloat_t x, const FvsFloat_t y,
       const FvsMinutiaType_t type, const FvsFloat_t angle)
{
    iFvsMinutiaSet_t* minutia = (iFvsMinutiaSet_t*)min;
    FvsError_t nRet = FvsOK;

    if (minutia->nbminutia < minutia->tablesize)
    {
        minutia->ptable[minutia->nbminutia].x       = x;
        minutia->ptable[minutia->nbminutia].y       = y;
        minutia->ptable[minutia->nbminutia].type    = type;
        minutia->ptable[minutia->nbminutia].angle   = angle;
        minutia->nbminutia++;
    }
    else
        /* no place left in table */
        nRet = FvsMemory;

    return nRet;
}


//taken from matching.c by petertu and adapted for angles from -M_PI/2 to M_PI/2
static inline FvsFloat_t anglediff(FvsFloat_t a, FvsFloat_t b)
{
  FvsFloat_t dif;

  dif = a-b;

  if (dif > -M_PI/2 && dif <= M_PI/2)
    return dif;
  else if (dif <= -M_PI/2)
    return (M_PI + dif);
  else
    return (M_PI - dif);
}

static FvsError_t MinutiaSetCheckClean(FvsMinutiaSet_t min)
{
    iFvsMinutiaSet_t* minutia = (iFvsMinutiaSet_t*)min;
    FvsError_t    nRet = FvsOK;
    FvsFloat_t    tx, ty; /* tolerances */
    FvsInt_t      i, j;
    FvsMinutia_t* mi, *mj;

    tx = 8.0;
    ty = 8.0;

    if (minutia!=NULL && minutia->nbminutia > 1)
    {
		// test if a minutia with these coordinates, type and
        //       angle is already in the table
        for (j = 0;   j < minutia->nbminutia; j++)
            for (i = j+1; i < minutia->nbminutia; i++)
            {
                mi = minutia->ptable + i;
                mj = minutia->ptable + j;

                // Minutia that are near each other and opposite directions -> Remove both

                if ( (fabs(mi->x     - mj->x    ) < tx     ) &&
                     (fabs(mi->y     - mj->y    ) < ty     ) &&
                     mi->type == FvsMinutiaTypeEnding &&
                     mj->type == FvsMinutiaTypeEnding &&
                     anglediff(mi->angle,mj->angle) < M_PI/8
                    )
                {
                    // found two line endings that are close to each other -> remove one at a time

                    if(i < minutia->nbminutia-1)
                    {
                        // swap mi with last non-dupilcate minutia
                        mi->x = minutia->ptable[minutia->nbminutia-1].x;
                        mi->y = minutia->ptable[minutia->nbminutia-1].y;
                        mi->angle = minutia->ptable[minutia->nbminutia-1].angle;
                        mi->type = minutia->ptable[minutia->nbminutia-1].type;
                    }
                    minutia->nbminutia--;

                    // do same for nj
                    if(j < minutia->nbminutia-1)
                    {
                        mj->x = minutia->ptable[minutia->nbminutia-1].x;
                        mj->y = minutia->ptable[minutia->nbminutia-1].y;
                        mj->angle = minutia->ptable[minutia->nbminutia-1].angle;
                        mj->type = minutia->ptable[minutia->nbminutia-1].type;
                    }
                    minutia->nbminutia--;
                }
            }
    }
    else
        /* no place left in table */
        nRet = FvsMemory;

    return nRet;
}


#define P(x,y)      p[(x)+(y)*pitch]


/* draw the mintuia set into the image */
FvsError_t MinutiaSetDraw(const FvsMinutiaSet_t min, FvsImage_t image)
{
    FvsInt_t w       = ImageGetWidth(image);
    FvsInt_t h       = ImageGetHeight(image);
    FvsInt_t pitch   = ImageGetPitch(image);
    FvsByte_t* p     = ImageGetBuffer(image);

    FvsInt_t n, x, y;
    FvsFloat_t fx, fy;
    FvsMinutia_t* minutia = MinutiaSetGetBuffer(min);
    FvsInt_t nbminutia    = MinutiaSetGetCount(min);

    if (minutia==NULL || p==NULL)
        return FvsMemory;

    /* draw each minutia */
    for (n = 0; n < nbminutia; n++)
    {
        x = (FvsInt_t)minutia[n].x;
        y = (FvsInt_t)minutia[n].y;
        if (x<w-5 && x>4)
        {
            if (y<h-5 && y>4)
            {
                switch (minutia[n].type)
                {
                case FvsMinutiaTypeEnding:
                    P(x,y)    = 0xFF;
                    P(x-1, y) = 0xA0;
                    P(x+1, y) = 0xA0;
                    P(x, y-1) = 0xA0;
                    P(x, y+1) = 0xA0;
                    break;
                case FvsMinutiaTypeBranching:
                    P(x,y)    = 0xFF;
                    P(x-1, y-1) = 0xA0;
                    P(x+1, y-1) = 0xA0;
                    P(x-1, y+1) = 0xA0;
                    P(x+1, y+1) = 0xA0;
                    break;
                default:
                    continue;
                }
                fx = sin(minutia[n].angle);
                fy = -cos(minutia[n].angle);
                P(x+(int32_t)(fx)    ,y+(int32_t)(fy)    ) = 0xFF;
                P(x+(int32_t)(fx*2.0),y+(int32_t)(fy*2.0)) = 0xFF;
                P(x+(int32_t)(fx*3.0),y+(int32_t)(fy*3.0)) = 0xFF;
                P(x+(int32_t)(fx*4.0),y+(int32_t)(fy*4.0)) = 0xFF;
                P(x+(int32_t)(fx*5.0),y+(int32_t)(fy*5.0)) = 0xFF;
            }
        }
    }
    return FvsOK;
}


/* defines to facilitate reading */
// note by petertu: caution: these defines differ from the ones in imagemanip.c
#define P1  P(x  ,y-1)
#define P2  P(x+1,y-1)
#define P3  P(x+1,y  )
#define P4  P(x+1,y+1)
#define P5  P(x  ,y+1)
#define P6  P(x-1,y+1)
#define P7  P(x-1,y  )
#define P8  P(x-1,y-1)


/*
** This function locates the minutia and compute their properties
*/
FvsError_t MinutiaSetExtract
    (
    FvsMinutiaSet_t       minutia,
    const FvsImage_t      image,
    const FvsFloatField_t direction,
    const FvsImage_t      mask
    )
{
    FvsInt_t w      = ImageGetWidth(image);
    FvsInt_t h      = ImageGetHeight(image);
    FvsInt_t pitch  = ImageGetPitch(image);
    FvsInt_t pitchm = ImageGetPitch(mask);
    FvsByte_t* p    = ImageGetBuffer(image);
    FvsByte_t* m    = ImageGetBuffer(mask);
    FvsInt_t   x, y;
    FvsFloat_t angle = 0.0;
    FvsInt_t   whitecount;

    if (m==NULL || p==NULL)
        return FvsMemory;

    (void)MinutiaSetEmpty(minutia);

    /* loop through the image and find the minutas */
    for (y = 1; y < h-1; y++)
    for (x = 1; x < w-1; x++)
    {
        if (m[x+y*pitchm]==0)
             continue;
        if (P(x,y)==0xFF)
        {
            whitecount = 0;
            if (P1!=0) whitecount++;
            if (P2!=0) whitecount++;
            if (P3!=0) whitecount++;
            if (P4!=0) whitecount++;
            if (P5!=0) whitecount++;
            if (P6!=0) whitecount++;
            if (P7!=0) whitecount++;
            if (P8!=0) whitecount++;

            switch(whitecount)
            {
            case 0:
                /* isolated point, ignore it */
                break;
            case 1:
                /* detect angle */
  	            angle = FloatFieldGetValue(direction, x, y);
                (void)MinutiaSetAdd(minutia, (FvsFloat_t)x, (FvsFloat_t)y,
                              FvsMinutiaTypeEnding, (FvsFloat_t)angle);
                break;
            case 2:
                break;
            default:
                /* normal line or branching */
                /* it may be a branching but we must make sure */
/*
                if ((P(x-1, y+1) && P(x-1, y-1)) ||
                    (P(x-1, y-1) && P(x+1, y-1)) ||
                    (P(x+1, y-1) && P(x+1, y+1)) ||
                    (P(x+1, y+1) && P(x-1, y+1)))
*/
  							if ((P1 && P2) || (P2 && P3) || (P3 && P4) || (P4 && P5) || (P5 && P6) || (P6 && P7) || (P7 && P8) || (P8 && P1))
								{
									//do nothing
								}
								else
                {
     	            angle = FloatFieldGetValue(direction, x, y);
                    (void)MinutiaSetAdd(minutia, (FvsFloat_t)x, (FvsFloat_t)y,
                                  FvsMinutiaTypeBranching, (FvsFloat_t)angle);
                }
                /* is branch double check */
                break;
            }
        }
    }
    (void)MinutiaSetCheckClean(minutia);

    return FvsOK;
}

static float subtractTimeSpec(struct timespec value1, struct timespec value2)
{
    long time1 = value1.tv_sec * 1000000000 + value1.tv_nsec;
    long time2 = value2.tv_sec * 1000000000 + value2.tv_nsec;

    return (float)(time2 - time1) / 1000000000;
}

static float sumFloats(float *array, unsigned int size)
{
    unsigned int i;
    float sum = 0;
    for (i = 0; i < size; i++)
    {
        sum += array[i];
    }
    return sum;
}

FvsMinutiaSet_t* loadMinutiaSetsFromFiles(int numberOfFiles, const char **listOfFiles)
{
    FvsMinutiaSet_t *minutiaArray = (FvsMinutiaSet_t*)malloc(numberOfFiles * sizeof(FvsMinutiaSet_t));
    struct timespec time1, time2;
    float elaspedTime, averageThreadTime, averageWallTime, *perThreadTime;
    perThreadTime = (float*)malloc(numberOfFiles * sizeof(float));
    clock_gettime(CLOCK_MONOTONIC_RAW, &time1);

#pragma omp parallel for
    for (int i = 0; i < numberOfFiles; ++i)
    {
        minutiaArray[i] = calculateMinutiaSet(listOfFiles[i], &perThreadTime[i]);
    }

    /* sort the sets, this code was removed from MatchingCompareMinutiaSets */
#pragma omp parallel for
    for (int i = 0; i < numberOfFiles; ++i)
        MinutiaSetSort(MinutiaSetGetBuffer(minutiaArray[i]), MinutiaSetGetCount(minutiaArray[i]));

    clock_gettime(CLOCK_MONOTONIC_RAW, &time2);
    elaspedTime = subtractTimeSpec(time1, time2);
    averageWallTime = elaspedTime / numberOfFiles;
    averageThreadTime = sumFloats(perThreadTime, numberOfFiles) / numberOfFiles;
    printf("Load %d files: %02.3fsec = %02.3fsec/file, cpu time: %02.3fsec, speedup: %02.2fx\n",
           numberOfFiles, elaspedTime, averageWallTime, averageThreadTime, averageThreadTime / averageWallTime);
    free(perThreadTime);
    return minutiaArray;
}

FvsFloat_t* compareMinutiaSets(int numberOfSets, const FvsMinutiaSet_t *minutiaArray)
{
    FvsFloat_t *goodness = (FvsFloat_t*)malloc(numberOfSets * sizeof(FvsFloat_t));
    goodness[0] = 1;            /* base set is always equal to itself */
    /* start loop at 1! */
#pragma omp parallel for
    for (int i = 1; i < numberOfSets; ++i)
    {
        MatchingCompareMinutiaSets(minutiaArray[0], minutiaArray[i], goodness + i);
    }
    return goodness;
}

void cleanupMinutiaSetArray(int numberOfSets, FvsMinutiaSet_t *minutiaArray)
{
    for (int i = 0; i < numberOfSets; ++i)
    {
        MinutiaSetDestroy(minutiaArray[i]);
    }
    free(minutiaArray);
}

static float subtractTimeval(struct timeval value2, struct timeval value1)
{
    long time1 = value1.tv_sec * 1000000 + value1.tv_usec;
    long time2 = value2.tv_sec * 1000000 + value2.tv_usec;

    return (float)(time2 - time1) / 1000000;
}

FvsMinutiaSet_t calculateMinutiaSet(const char *file, float *thread_time)
{
    FvsImage_t image;
    FvsImage_t mask;
    FvsFloatField_t direction;
    FvsMinutiaSet_t minutia;
    FvsFloatField_t frequency;
    struct rusage usage1, usage2;
    getrusage(RUSAGE_THREAD, &usage1);

    mask      = ImageCreate();
    image     = ImageCreate();
    direction = FloatFieldCreate();
    frequency = FloatFieldCreate();
    minutia   = MinutiaSetCreate(100);

    if (mask!=NULL && image!=NULL && direction!=NULL && minutia!=NULL && frequency!=NULL)
    {
        FvsImageImport(image, file);
        ImageSoftenMean(image, 3);
        ImagePixelNormalize(image, 9);
        // creates a fixed-size mask, which is needed to ignore the mostly
        // unusable border areas in an image
        FingerprintGetFixedMask(image, mask, 6);
        FingerprintGetDirection(image, direction, 5, 4);
        // apply mask to image
        ImageLogical(image,mask, FvsLogicalAnd);
        FingerprintGetFrequency(image, direction, frequency);
        ImageEnhanceGabor(image, direction, frequency, mask, 4.0);
        // Binarize the image
        ImageBinarize(image, (FvsByte_t)0x80);
        // Thinning: Fist the standard fvs algorithm (ImageThinHitMiss) is
        // used, then the ImageEnhanceThinned algorithm written by petertu is
        // applied
        ImageThinHitMiss(image);
        ImageEnhanceThinned(image, direction);
        // detecting the minutia in the fingerprint and writing them to minutia
        // list
        MinutiaSetExtract(minutia, image, direction, mask);
        // this function draws the minutias as small vectors. the contents of
        // "image" are erased first
        ImageClear(image);
        MinutiaSetDraw(minutia, image);
    }
    ImageDestroy(image);
    ImageDestroy(mask);
    FloatFieldDestroy(direction);

    getrusage(RUSAGE_THREAD, &usage2);
    *thread_time = subtractTimeval(usage2.ru_utime, usage1.ru_utime);
    return minutia;
}

FvsFloat_t* calculateGoodnesses(int numberOfFiles, const char **listOfFiles)
{
    FvsMinutiaSet_t *minutiaArray = loadMinutiaSetsFromFiles(numberOfFiles, listOfFiles);
    FvsFloat_t *goodness = compareMinutiaSets(numberOfFiles, minutiaArray);
    cleanupMinutiaSetArray(numberOfFiles, minutiaArray);
    return goodness;
}
